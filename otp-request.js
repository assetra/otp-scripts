var moment = require('moment');
var request = require('request');
var querystring = require('querystring');
var log   = require('debug')('app:otp-request:log'),
    error = require('debug')('app:otp-request:error');

var otpBase = process.env.OTP_BASE || 'http://otp.assetra.net:5000';

function doOTPRequest(origin, destination, search, callback) {
  var query = {
    fromPlace: origin,
    toPlace: destination,
    numItineraries: 1,
  };

  if( search.time_of && search.time_of.substr(0,3) === 'arr' ) {
    query.arriveBy = 1;
  }

  var time = moment();
  if( search.time ) {
    time = moment.unix(search.time);
    query.time = time.format();
  }

  var url = otpBase + '/otp/routers/default/plan?' + querystring.stringify(query);
  log('Hitting OTP online with', url);

  request({
    url: url,
    json: true
  }, function(err, response, body) {
    /* istanbul ignore if */
    if(err) {
      return callback(err);
    }

    if( body && body.error) {
      // no journeys found..
      log('No routes found.');
      return callback(null, null);
    }

    var itinerary = body && body.plan && body.plan.itineraries && body.plan.itineraries.length ? body.plan.itineraries[0] : {};
    // var duration = itinerary.duration;
    var endTime = Math.round( itinerary.endTime / 1000 );

    /* istanbul ignore if */
    if( ! endTime ) {
      // no journeys found..
      error('Unexpected output.', body);
      return callback(new Error('Unexpected output.'), null);
    }

    callback(null, endTime-time.unix());
  });
}

module.exports = doOTPRequest;

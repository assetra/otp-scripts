#! /bin/bash

add-apt-repository ppa:openjdk-r/ppa -y
apt-get update
apt-get install openjdk-8-jdk awscli -y
update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

mkdir /usr/local/otp
cd /usr/local/otp
wget http://dev.opentripplanner.org/jars/otp-0.18.0.jar
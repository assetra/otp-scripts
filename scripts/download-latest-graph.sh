#! /bin/bash

# setup the directory that will contain the graph
rm -rf /usr/local/otp/graphs
mkdir /usr/local/otp/graphs

# pull down the latest graph from S3
aws s3 cp s3://um-otp-graphs/latest-london-uk.tar.gz /usr/local/otp/graphs/latest-london-uk.tar.gz

# unarchive it
cd /usr/local/otp/graphs
mkdir london
tar -xf latest-london-uk.tar.gz london